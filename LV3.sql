--Prvi zadatak:
use stuslu;
select upper(substring(ime,1,1))+upper(substring(prezime,1,1)) as inicijali, year(datum_rodjenja) as "godina rodjenja" from student;
select * from student order by datum_rodjenja asc;
select ime,prezime from student where datum_rodjenja=min((select dat_rodjenja from student)) and spol='Z';

--Drugi zadatak:
use stuslu;
select count(oib) as "Broj studenata", count(distinct idmjesta) as "Broj mjesta" from student;

--Treci zadatak:
use stuslu;
SELECT avg(ocjena) from ispit where ocjena>1;

--Cetvrti zadatak:
use stuslu;
select ime,prezime,avg(ocjena) from student,ispit where student.matbr=ispit.matbr and ocjena>1 order by avg(ocjena) desc;
select student.matbr from ispit,student where avg((select ocjena from ispit where ispit.matbr=student.matbr))>2.5;

--Peti zadatak:
use stuslu;
create view ispiti_po_studentima as select student.ime,student.prezime,predmet.naziv,ispit.ocjena from student,predmet,ispit where student.matbr=ispit.matbr and ispit.sifra_predmeta=predmet.sifra_predmeta;
